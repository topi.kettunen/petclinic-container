FROM ubuntu:xenial

RUN apt-get update && apt-get install -y \
        git \
        maven \
        openjdk-8-jdk \
        --no-install-recommends \
        && rm -rf /var/lib/apt/lists/*

RUN git clone https://github.com/spring-projects/spring-petclinic.git /spring-petclinic && \
    cd /spring-petclinic && \
    mvn clean install

COPY entrypoint.sh entrypoint.sh
RUN chmod ugo+x entrypoint.sh

ENV NAME=petclinic

COPY application.properties /spring-petclinic/src/main/resources/application.properties

ENTRYPOINT [ "/entrypoint.sh" ]
